FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/workflow-service/src
COPY pom.xml /home/workflow-service
RUN mvn -f /home/workflow-service/pom.xml clean package

FROM adoptopenjdk/openjdk11:alpine
COPY --from=build /home/workflow-service/target/workflow-service.jar /usr/local/lib/workflow-service.jar
EXPOSE 8087
ENTRYPOINT ["java","-jar","/usr/local/lib/workflow-service.jar"]