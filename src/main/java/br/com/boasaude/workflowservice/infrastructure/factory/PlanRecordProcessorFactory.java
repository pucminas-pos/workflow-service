package br.com.boasaude.workflowservice.infrastructure.factory;

import br.com.boasaude.workflowservice.domain.processor.PlanStreamProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessorFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PlanRecordProcessorFactory implements IRecordProcessorFactory {

    private final PlanStreamProcessor planStreamProcessor;

    @Override
    public IRecordProcessor createProcessor() {
        return planStreamProcessor;
    }
}
