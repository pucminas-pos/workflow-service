package br.com.boasaude.workflowservice.infrastructure.factory;

import br.com.boasaude.workflowservice.domain.processor.ReplyStreamProcessor;
import br.com.boasaude.workflowservice.domain.processor.RequestStreamProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessorFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RequestRecordProcessorFactory implements IRecordProcessorFactory {

    private final RequestStreamProcessor requestStreamProcessor;

    @Override
    public IRecordProcessor createProcessor() {
        return requestStreamProcessor;
    }
}
