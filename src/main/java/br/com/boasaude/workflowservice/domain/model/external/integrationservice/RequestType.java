package br.com.boasaude.workflowservice.domain.model.external.integrationservice;

public enum RequestType {

    COMPLAINT, SUGGESTION, COMPLIMENT;


}
