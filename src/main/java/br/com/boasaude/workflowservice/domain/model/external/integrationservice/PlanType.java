package br.com.boasaude.workflowservice.domain.model.external.integrationservice;

public enum PlanType {

    MEDICAL, MEDICAL_ODONTOLOGY

}
