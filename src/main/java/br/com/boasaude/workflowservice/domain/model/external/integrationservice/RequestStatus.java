package br.com.boasaude.workflowservice.domain.model.external.integrationservice;

public enum RequestStatus {

    OPENED, IN_PROGRESS, CLOSED;

}
