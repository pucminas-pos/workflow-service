package br.com.boasaude.workflowservice.domain.model.external.integrationservice;

public enum PlanStatus {

    ACTIVE, SUSPENDED, INACTIVE

}
