package br.com.boasaude.workflowservice.domain.model.external.integrationservice;

public enum PlanCategory {

    BASIC, INTERMEDIATE, VIP

}
