package br.com.boasaude.workflowservice.domain.model.external.integrationservice;

public enum PlanName {

    INDIVIDUAL, CORPORATE;

}
